-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 04 2020 г., 13:10
-- Версия сервера: 5.6.41
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `hh`
--

-- --------------------------------------------------------

--
-- Структура таблицы `address`
--

CREATE TABLE `address` (
  `idAddress` int(11) NOT NULL,
  `City` varchar(50) DEFAULT NULL,
  `Street` varchar(50) DEFAULT NULL,
  `Building` varchar(10) DEFAULT NULL,
  `Description` varchar(400) DEFAULT NULL,
  `Lat` double DEFAULT NULL,
  `Lng` double DEFAULT NULL,
  `Raw` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1820 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `address`
--

INSERT INTO `address` (`idAddress`, `City`, `Street`, `Building`, `Description`, `Lat`, `Lng`, `Raw`) VALUES
(679675, 'Коломна', 'улица Октябрьской Революции', '362', '', 55.08408, 38.800142, 'Коломна, улица Октябрьской Революции, 362'),
(807381, 'Москва', 'Краснопресненская набережная', '', '', 55.753752, 37.558832, 'Москва, Краснопресненская набережная'),
(948854, 'Москва', '3-я Рыбинская улица', '18к1с1', '', 55.789957, 37.660197, 'Москва, 3-я Рыбинская улица, 18к1с1'),
(1588276, 'Санкт-Петербург', 'Боровая улица', '47', '', 59.913722, 30.340976, 'Санкт-Петербург, Боровая улица, 47'),
(1699035, 'Москва', 'Угрешская улица', '2с1', '', 55.711356, 37.683527, 'Москва, Угрешская улица, 2с1'),
(2825975, 'Кудымкар', 'улица Данилова', '15Б', '', 59.019055, 54.65979, 'Кудымкар, улица Данилова, 15Б'),
(2942562, 'Санкт-Петербург', 'Невский проспект', '110', '', 59.931715, 30.357918, 'Санкт-Петербург, Невский проспект, 110'),
(3205119, 'Краснодар', 'улица Стасова', '176', '', 45.028438, 39.044052, 'Краснодар, улица Стасова, 176');

-- --------------------------------------------------------

--
-- Структура таблицы `area`
--

CREATE TABLE `area` (
  `idArea` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Url` varchar(300) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2048 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `area`
--

INSERT INTO `area` (`idArea`, `Name`, `Url`) VALUES
(0, '', ''),
(1, 'Москва', 'https://api.hh.ru/areas/1'),
(2, 'Санкт-Петербург', 'https://api.hh.ru/areas/2'),
(3, 'Екатеринбург', 'https://api.hh.ru/areas/3'),
(4, 'Новосибирск', 'https://api.hh.ru/areas/4'),
(10, 'Горно-Алтайск', 'https://api.hh.ru/areas/10'),
(11, 'Барнаул', 'https://api.hh.ru/areas/11'),
(17, 'Белгород', 'https://api.hh.ru/areas/17'),
(20, 'Улан-Удэ', 'https://api.hh.ru/areas/20'),
(24, 'Волгоград', 'https://api.hh.ru/areas/24'),
(35, 'Иркутск', 'https://api.hh.ru/areas/35'),
(46, 'Черкесск', 'https://api.hh.ru/areas/46'),
(47, 'Кемерово', 'https://api.hh.ru/areas/47'),
(49, 'Киров', 'https://api.hh.ru/areas/49'),
(53, 'Краснодар', 'https://api.hh.ru/areas/53'),
(54, 'Красноярск', 'https://api.hh.ru/areas/54'),
(58, 'Липецк', 'https://api.hh.ru/areas/58'),
(61, 'Йошкар-Ола', 'https://api.hh.ru/areas/61'),
(63, 'Саранск', 'https://api.hh.ru/areas/63'),
(68, 'Омск', 'https://api.hh.ru/areas/68'),
(70, 'Оренбург', 'https://api.hh.ru/areas/70'),
(71, 'Пенза', 'https://api.hh.ru/areas/71'),
(72, 'Пермь', 'https://api.hh.ru/areas/72'),
(76, 'Ростов-на-Дону', 'https://api.hh.ru/areas/76'),
(78, 'Самара', 'https://api.hh.ru/areas/78'),
(79, 'Саратов', 'https://api.hh.ru/areas/79'),
(82, 'Владикавказ', 'https://api.hh.ru/areas/82'),
(84, 'Ставрополь', 'https://api.hh.ru/areas/84'),
(88, 'Казань', 'https://api.hh.ru/areas/88'),
(90, 'Томск', 'https://api.hh.ru/areas/90'),
(92, 'Тула', 'https://api.hh.ru/areas/92'),
(95, 'Тюмень', 'https://api.hh.ru/areas/95'),
(96, 'Ижевск', 'https://api.hh.ru/areas/96'),
(98, 'Ульяновск', 'https://api.hh.ru/areas/98'),
(99, 'Уфа', 'https://api.hh.ru/areas/99'),
(104, 'Челябинск', 'https://api.hh.ru/areas/104'),
(112, 'Ярославль', 'https://api.hh.ru/areas/112'),
(115, 'Киев', 'https://api.hh.ru/areas/115'),
(120, 'Запорожье', 'https://api.hh.ru/areas/120'),
(125, 'Львов', 'https://api.hh.ru/areas/125'),
(130, 'Севастополь', 'https://api.hh.ru/areas/130'),
(150, 'Аксай (Казахстан)', 'https://api.hh.ru/areas/150'),
(153, 'Атырау', 'https://api.hh.ru/areas/153'),
(154, 'Актобе', 'https://api.hh.ru/areas/154'),
(159, 'Нур-Султан', 'https://api.hh.ru/areas/159'),
(160, 'Алматы', 'https://api.hh.ru/areas/160'),
(180, 'Петропавловск', 'https://api.hh.ru/areas/180'),
(181, 'Павлодар', 'https://api.hh.ru/areas/181'),
(185, 'Семей', 'https://api.hh.ru/areas/185'),
(194, 'Усть-Каменогорск', 'https://api.hh.ru/areas/194'),
(205, 'Шымкент', 'https://api.hh.ru/areas/205'),
(237, 'Сочи', 'https://api.hh.ru/areas/237'),
(301, 'Обнинск', 'https://api.hh.ru/areas/301'),
(1002, 'Минск', 'https://api.hh.ru/areas/1002'),
(1003, 'Гомель', 'https://api.hh.ru/areas/1003'),
(1006, 'Гродно', 'https://api.hh.ru/areas/1006'),
(1007, 'Брест', 'https://api.hh.ru/areas/1007'),
(1130, 'Братск', 'https://api.hh.ru/areas/1130'),
(1209, 'Куйбышев', 'https://api.hh.ru/areas/1209'),
(1220, 'Бийск', 'https://api.hh.ru/areas/1220'),
(1237, 'Мариинск', 'https://api.hh.ru/areas/1237'),
(1240, 'Новокузнецк', 'https://api.hh.ru/areas/1240'),
(1243, 'Прокопьевск', 'https://api.hh.ru/areas/1243'),
(1285, 'Красноуфимск', 'https://api.hh.ru/areas/1285'),
(1296, 'Первоуральск', 'https://api.hh.ru/areas/1296'),
(1301, 'Серов', 'https://api.hh.ru/areas/1301'),
(1319, 'Березники', 'https://api.hh.ru/areas/1319'),
(1328, 'Кудымкар', 'https://api.hh.ru/areas/1328'),
(1330, 'Лысьва', 'https://api.hh.ru/areas/1330'),
(1440, 'Анапа', 'https://api.hh.ru/areas/1440'),
(1442, 'Армавир', 'https://api.hh.ru/areas/1442'),
(1444, 'Геленджик', 'https://api.hh.ru/areas/1444'),
(1447, 'Ейск', 'https://api.hh.ru/areas/1447'),
(1450, 'Крымск', 'https://api.hh.ru/areas/1450'),
(1454, 'Новороссийск', 'https://api.hh.ru/areas/1454'),
(1456, 'Славянск-на-Кубани', 'https://api.hh.ru/areas/1456'),
(1516, 'Камышин', 'https://api.hh.ru/areas/1516'),
(1546, 'Новошахтинск', 'https://api.hh.ru/areas/1546'),
(1571, 'Орск', 'https://api.hh.ru/areas/1571'),
(1594, 'Сызрань', 'https://api.hh.ru/areas/1594'),
(1730, 'Муром', 'https://api.hh.ru/areas/1730'),
(2032, 'Клин', 'https://api.hh.ru/areas/2032'),
(2033, 'Коломна', 'https://api.hh.ru/areas/2033'),
(2035, 'Видное', 'https://api.hh.ru/areas/2035'),
(2039, 'Люберцы', 'https://api.hh.ru/areas/2039'),
(2040, 'Можайск', 'https://api.hh.ru/areas/2040'),
(2060, 'Климовск', 'https://api.hh.ru/areas/2060'),
(2061, 'Подольск (Московская область)', 'https://api.hh.ru/areas/2061'),
(2094, 'Реутов', 'https://api.hh.ru/areas/2094'),
(2289, 'Речица', 'https://api.hh.ru/areas/2289'),
(2386, 'Джанкой', 'https://api.hh.ru/areas/2386'),
(2492, 'Баку', 'https://api.hh.ru/areas/2492'),
(2759, 'Ташкент', 'https://api.hh.ru/areas/2759'),
(2760, 'Бишкек', 'https://api.hh.ru/areas/2760'),
(2782, 'Фергана', 'https://api.hh.ru/areas/2782'),
(3710, 'Иглино', 'https://api.hh.ru/areas/3710');

-- --------------------------------------------------------

--
-- Структура таблицы `contacts`
--

CREATE TABLE `contacts` (
  `idContacts` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `idPhones` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contacts`
--

INSERT INTO `contacts` (`idContacts`, `Name`, `Email`, `idPhones`) VALUES
(1, 'Савичева Дарья Викторовна', 'dvsavicheva@rnd.beeline.ru', NULL),
(2, 'Курятова Анна', 'annet.kuryatova@gmail.com', NULL),
(3, 'Стрельникова Елена Анатольевна', 'strelnikova@psk.expert', NULL),
(4, ' Мария ', '', NULL),
(5, 'Крючкова Анастасия Андреевна', 'kryuchkova.aa2@dns-shop.ru', NULL),
(6, 'Semen Astanin', 'semen.astanin@hillcorp.kz', NULL),
(7, 'Александрова Ольга Валентиновна', '', NULL),
(9, 'Специалист группы подбора персонала', 'pfusmano@mtsretail.ru', NULL),
(10, 'Semen Astanin', 'semen.astanin@hillcorp.kz', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--

CREATE TABLE `department` (
  `idDepartment` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `iddDepartment` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`idDepartment`, `Name`, `iddDepartment`) VALUES
(1, 'Билайн: Офисы продаж', 'bil-4934-ofsale'),
(6, 'IKEA. Магазины ', '4908-4908-mag');

-- --------------------------------------------------------

--
-- Структура таблицы `employer`
--

CREATE TABLE `employer` (
  `idEmployer` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Url` varchar(500) DEFAULT NULL,
  `AlternateUrl` varchar(500) DEFAULT NULL,
  `idLogoUrls` int(11) DEFAULT NULL,
  `VacanciesUrl` varchar(500) DEFAULT NULL,
  `Trusted` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `employer`
--

INSERT INTO `employer` (`idEmployer`, `Name`, `Url`, `AlternateUrl`, `idLogoUrls`, `VacanciesUrl`, `Trusted`) VALUES
(80, 'Альфа-Банк', 'https://api.hh.ru/employers/80', 'https://hh.ru/employer/80', 8, 'https://api.hh.ru/vacancies?employer_id=80', 1),
(404, 'ЗЕНИТ, банк', 'https://api.hh.ru/employers/404', 'https://hh.ru/employer/404', 3, 'https://api.hh.ru/vacancies?employer_id=404', 1),
(2800, 'Медиагруппа «Актион-МЦФЭР»', 'https://api.hh.ru/employers/2800', 'https://hh.ru/employer/2800', 5, 'https://api.hh.ru/vacancies?employer_id=2800', 1),
(4908, 'IKEA', 'https://api.hh.ru/employers/4908', 'https://hh.ru/employer/4908', 6, 'https://api.hh.ru/vacancies?employer_id=4908', 1),
(4934, 'Билайн', 'https://api.hh.ru/employers/4934', 'https://hh.ru/employer/4934', 1, 'https://api.hh.ru/vacancies?employer_id=4934', 1),
(20327, 'Zara', 'https://api.hh.ru/employers/20327', 'https://hh.ru/employer/20327', 1, 'https://api.hh.ru/vacancies?employer_id=20327', 1),
(38697, 'Янино, Молочный завод', 'https://api.hh.ru/employers/38697', 'https://hh.ru/employer/38697', 7, 'https://api.hh.ru/vacancies?employer_id=38697', 1),
(54979, 'АШАН, Сеть Гипермаркетов', 'https://api.hh.ru/employers/54979', 'https://hh.ru/employer/54979', 3, 'https://api.hh.ru/vacancies?employer_id=54979', 1),
(135860, 'Юнисервис', 'https://api.hh.ru/employers/135860', 'https://hh.ru/employer/135860', 6, 'https://api.hh.ru/vacancies?employer_id=135860', 1),
(569201, 'High Industrial Lubricants & Liquids (HILL) Corpor', 'https://api.hh.ru/employers/569201', 'https://hh.ru/employer/569201', 10, 'https://api.hh.ru/vacancies?employer_id=569201', 1),
(665449, 'Строительный Двор', 'https://api.hh.ru/employers/665449', 'https://hh.ru/employer/665449', 2, 'https://api.hh.ru/vacancies?employer_id=665449', 1),
(766785, 'Меркор-ПРУФ', 'https://api.hh.ru/employers/766785', 'https://hh.ru/employer/766785', 2, 'https://api.hh.ru/vacancies?employer_id=766785', 1),
(869227, 'Лучшее Время (ООО Оникс)', 'https://api.hh.ru/employers/869227', 'https://hh.ru/employer/869227', 4, 'https://api.hh.ru/vacancies?employer_id=869227', 1),
(1025275, 'Сеть магазинов цифровой и бытовой техники DNS', 'https://api.hh.ru/employers/1025275', 'https://hh.ru/employer/1025275', 5, 'https://api.hh.ru/vacancies?employer_id=1025275', 1),
(1075155, 'ЛокоТех', 'https://api.hh.ru/employers/1075155', 'https://hh.ru/employer/1075155', 4, 'https://api.hh.ru/vacancies?employer_id=1075155', 1),
(1131694, 'Empire Group Co.', 'https://api.hh.ru/employers/1131694', 'https://hh.ru/employer/1131694', 9, 'https://api.hh.ru/vacancies?employer_id=1131694', 1),
(2537115, 'Розничная сеть МТС', 'https://api.hh.ru/employers/2537115', 'https://hh.ru/employer/2537115', 9, 'https://api.hh.ru/vacancies?employer_id=2537115', 1),
(2711518, 'Партнеры Застройщиков', 'https://api.hh.ru/employers/2711518', 'https://hh.ru/employer/2711518', 10, 'https://api.hh.ru/vacancies?employer_id=2711518', 1),
(2801743, 'Релофт', 'https://api.hh.ru/employers/2801743', 'https://hh.ru/employer/2801743', 8, 'https://api.hh.ru/vacancies?employer_id=2801743', 1),
(2996166, 'Эксперт Спецодежда', 'https://api.hh.ru/employers/2996166', 'https://hh.ru/employer/2996166', 3, 'https://api.hh.ru/vacancies?employer_id=2996166', 1),
(3192913, 'КОМПЬЮТЕРНАЯ АКАДЕМИЯ ШАГ', 'https://api.hh.ru/employers/3192913', 'https://hh.ru/employer/3192913', 8, 'https://api.hh.ru/vacancies?employer_id=3192913', 1),
(3290933, 'ТЕХСТРОЙКАПИТАЛ', 'https://api.hh.ru/employers/3290933', 'https://hh.ru/employer/3290933', 7, 'https://api.hh.ru/vacancies?employer_id=3290933', 1),
(3549703, 'Курятова Анна Валерьевна', 'https://api.hh.ru/employers/3549703', 'https://hh.ru/employer/3549703', 2, 'https://api.hh.ru/vacancies?employer_id=3549703', 0),
(3602814, 'КСК ТРЕЙД', 'https://api.hh.ru/employers/3602814', 'https://hh.ru/employer/3602814', 9, 'https://api.hh.ru/vacancies?employer_id=3602814', 1),
(3646055, 'Премьер-Консалт', 'https://api.hh.ru/employers/3646055', 'https://hh.ru/employer/3646055', 10, 'https://api.hh.ru/vacancies?employer_id=3646055', 1),
(4488534, 'Prodektkompany', 'https://api.hh.ru/employers/4488534', 'https://hh.ru/employer/4488534', 1, 'https://api.hh.ru/vacancies?employer_id=4488534', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `insider_interview`
--

CREATE TABLE `insider_interview` (
  `idInsiderInterview` int(11) NOT NULL,
  `Url` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `logo_urls`
--

CREATE TABLE `logo_urls` (
  `idLogoUrls` int(11) NOT NULL,
  `240` varchar(500) DEFAULT NULL,
  `90` varchar(500) DEFAULT NULL,
  `Original` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `logo_urls`
--

INSERT INTO `logo_urls` (`idLogoUrls`, `240`, `90`, `Original`) VALUES
(1, 'https://hhcdn.ru/employer-logo/3155117.jpeg', 'https://hhcdn.ru/employer-logo/3155116.jpeg', 'https://hhcdn.ru/employer-logo-original/678496.jpg'),
(2, 'https://hhcdn.ru/employer-logo/516993.jpeg', 'https://hhcdn.ru/employer-logo/516992.jpeg', 'https://hhcdn.ru/employer-logo-original/140754.jpg'),
(3, 'https://hhcdn.ru/employer-logo/3052401.png', 'https://hhcdn.ru/employer-logo/3052400.png', 'https://hhcdn.ru/employer-logo-original/652813.png'),
(4, 'https://hhcdn.ru/employer-logo/1407306.jpeg', 'https://hhcdn.ru/employer-logo/1407305.jpeg', 'https://hhcdn.ru/employer-logo-original/241157.jpg'),
(5, 'https://hhcdn.ru/employer-logo/2415210.png', 'https://hhcdn.ru/employer-logo/2415209.png', 'https://hhcdn.ru/employer-logo-original/493364.png'),
(6, 'https://hhcdn.ru/employer-logo/289184.png', 'https://hhcdn.ru/employer-logo/289042.png', 'https://hhcdn.ru/employer-logo-original/231276.gif'),
(7, '', '', ''),
(8, 'https://hhcdn.ru/employer-logo/3096625.png', 'https://hhcdn.ru/employer-logo/3096624.png', 'https://hhcdn.ru/employer-logo-original/663873.png'),
(9, '', '', ''),
(10, '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `metro`
--

CREATE TABLE `metro` (
  `idMetro` int(11) NOT NULL,
  `Station_name` varchar(255) DEFAULT NULL,
  `Line_Name` varchar(255) DEFAULT NULL,
  `Station_Id` double DEFAULT NULL,
  `Line_Id` int(11) DEFAULT NULL,
  `Lat` double DEFAULT NULL,
  `Lng` double DEFAULT NULL,
  `idAddress` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=2730 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `metro`
--

INSERT INTO `metro` (`idMetro`, `Station_name`, `Line_Name`, `Station_Id`, `Line_Id`, `Lat`, `Lng`, `idAddress`) VALUES
(1, 'Маяковская', 'Невско-Василеостровская', 16.23, 16, 59.931366, 30.354645, 2942562),
(2, 'Улица 1905 года', 'Таганско-Краснопресненская', 7.146, 7, 55.763944, 37.562271, 807381),
(4, 'Кожуховская', 'Люблинско-Дмитровская', 10.52, 10, 55.706156, 37.68544, 1699035);

-- --------------------------------------------------------

--
-- Структура таблицы `metrostations`
--

CREATE TABLE `metrostations` (
  `idMetroStations` int(11) NOT NULL,
  `Stations_Name` varchar(50) DEFAULT NULL,
  `Line_Name` varchar(50) DEFAULT NULL,
  `Station_Id` double DEFAULT NULL,
  `Line_Id` int(11) DEFAULT NULL,
  `Lat` double DEFAULT NULL,
  `Lng` double DEFAULT NULL,
  `idAddress` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1092 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `metrostations`
--

INSERT INTO `metrostations` (`idMetroStations`, `Stations_Name`, `Line_Name`, `Station_Id`, `Line_Id`, `Lat`, `Lng`, `idAddress`) VALUES
(1, 'Маяковская', 'Невско-Василеостровская', 16.23, 16, 59.931366, 30.354645, 2942562),
(2, 'Владимирская', 'Кировско-Выборгская', 14.2, 14, 59.927628, 30.347898, 2942562),
(3, 'Площадь Восстания', 'Кировско-Выборгская', 14.199, 14, 59.930279, 30.361069, 2942562);

-- --------------------------------------------------------

--
-- Структура таблицы `phones`
--

CREATE TABLE `phones` (
  `idPhones` int(11) NOT NULL,
  `Comment` varchar(255) DEFAULT NULL,
  `City` int(11) DEFAULT NULL,
  `Number` int(11) DEFAULT NULL,
  `Country` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `salary`
--

CREATE TABLE `salary` (
  `idSalary` int(11) NOT NULL,
  `From` int(11) DEFAULT NULL,
  `To` int(11) DEFAULT NULL,
  `Currency` varchar(50) DEFAULT NULL,
  `Gross` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=1170 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `salary`
--

INSERT INTO `salary` (`idSalary`, `From`, `To`, `Currency`, `Gross`) VALUES
(1, 45000, 105000, 'RUR', 1),
(2, 50000, 0, 'RUR', 0),
(3, 100000, 150000, 'RUR', 0),
(4, 150000, 300000, 'RUR', 0),
(5, 35000, 60000, 'RUR', 0),
(7, 0, 230000, 'KZT', 0),
(8, 60000, 0, 'RUR', 0),
(9, 20000, 25000, 'RUR', 0),
(10, 85000, 0, 'RUR', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `snippet`
--

CREATE TABLE `snippet` (
  `idSnippet` int(11) NOT NULL,
  `Requirment` varchar(500) DEFAULT NULL,
  `Responsibility` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `snippet`
--

INSERT INTO `snippet` (`idSnippet`, `Requirment`, `Responsibility`) VALUES
(1, 'Специальные условия для кандидатов, имеющих опыт работы в сфере информационных технологий от полугода: Каждый сотрудник соблюдает Кодекс поведения, внутренние правила...', ''),
(2, 'Высшее юридическое образование. Опыт работы от 1 года юристом, ассистентом, помощником юридического отдела. Опыт работы с договорами или со схожими...', 'Юридическая экспертиза и последующее визирование договоров, соглашений, протоколов разногласий (договоры строительного подряда, поставки, договоры на выполнение работ, оказание услуг, соглашения...'),
(3, 'Высшее образование. Успешный опыт на руководящей позиции (желательно банковская/финансовая сфера. Опыт работы в обслуживании физических лиц и юридических лиц. ', 'Руководство работой Дополнительного офиса Банка. Анализ результатов деятельности Дополнительного офиса и разработка мероприятий по улучшению плановых показателей. Обеспечение и контроль...'),
(4, 'Высшее профильное образование (финансовое / экономическое / бухгалтерское). Знание основ бухгалтерского и налогового учета. Знания и опыт в области финансового и стратегического...', 'Управленческая отчетность, материалы для ГД и управляющей компании. Бюджетирование, отчетность об исполнении бюджета. Стратегическое и финансовое планирование. Финансовый контроль и...'),
(5, 'Ориентирование на потребности клиента.', 'Консультирование клиентов по всему ассортименту магазина(компьютерной, цифровой и бытовой техники). Развитие и поддержание долгосрочных отношений с клиентами. '),
(6, 'Умение стратегически видеть и оценивать влияние своей работы на компанию в целом. Развитые лидерские компетенции, дающие возможность уверенно взаимодействовать как...', 'Разрабатывать стратегию продаж по корпоративным клиентам в целях обеспечения долгосрочного развития бизнеса на вверенной территории через развитие коммерческого предложения ключевым...'),
(7, 'Навыки коммуникаций; навыки работы с документами; аккуратность.', 'Выполнение поручений непосредственного руководителя; работа с документами; составление базы клиентов; осуществление телефонных переговоров.'),
(8, 'Опыт работы менеджером проектов в крупной компании не менее 3-х лет. Опыт одновременного управления разработкой нескольких высоконагруженных проектов. ', 'Управлять проектами и проектными рисками. Разрабатывать план работ и технические задания. Вести проектную документацию. Обеспечивать своевременную реализацию проектов и контролировать...'),
(9, 'Внимательность, ответственность, высока скорость печати, грамотное письмо. Уверенный пользователь ПК, Интернет.', '1. работа с каталогами (перенос информации из pdf в exel и на сайт) работа ручная и требует большого внимания). 2. '),
(10, 'Рассматриваем кандидатов с желанием релоцироваться из России в Казахстан. Мы ищем кандидата с опытом работы в продажах/развитии бизнеса и...', 'Основная задача HR - прямое участие в увеличении численности филиала компании, а так же организация адаптации персонала. Для интеграции кандидата в...');

-- --------------------------------------------------------

--
-- Структура таблицы `type`
--

CREATE TABLE `type` (
  `idType` int(11) NOT NULL,
  `idTypes` varchar(30) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type`
--

INSERT INTO `type` (`idType`, `idTypes`, `Name`) VALUES
(0, 'open', 'Открытая'),
(1, 'anonymous', 'Анонимная');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `name`, `login`, `pass`) VALUES
(1, 'Александр', 'nas1993166', '$2y$12$VTFxUTlVU2Vlb1NEVXZLc.HehtQgwCPHnxnGV7Ro2YcEIE3zo.fKG');

-- --------------------------------------------------------

--
-- Структура таблицы `vacancies`
--

CREATE TABLE `vacancies` (
  `idVacancies` int(11) NOT NULL,
  `Premium` tinyint(1) DEFAULT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `idDepartment` int(11) DEFAULT NULL,
  `HasTest` tinyint(1) DEFAULT NULL,
  `ResponseLetterRequired` tinyint(1) DEFAULT NULL,
  `idArea` int(11) DEFAULT NULL,
  `idSalary` int(11) DEFAULT NULL,
  `idType` int(11) DEFAULT NULL,
  `idAddress` int(11) DEFAULT NULL,
  `ResponseUrl` varchar(500) DEFAULT NULL,
  `SortPointDistanse` int(11) DEFAULT NULL,
  `idEmployer` int(11) DEFAULT NULL,
  `PublishedAt` datetime DEFAULT NULL,
  `Created_At` datetime DEFAULT NULL,
  `Archived` tinyint(1) DEFAULT NULL,
  `ApplyAlternateUrl` varchar(500) DEFAULT NULL,
  `idInsiderInterview` int(11) DEFAULT NULL,
  `Url` varchar(500) DEFAULT NULL,
  `AlternateUrl` varchar(500) DEFAULT NULL,
  `idSnippet` int(11) DEFAULT NULL,
  `idContacts` int(11) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=819 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `vacancies`
--

INSERT INTO `vacancies` (`idVacancies`, `Premium`, `Name`, `idDepartment`, `HasTest`, `ResponseLetterRequired`, `idArea`, `idSalary`, `idType`, `idAddress`, `ResponseUrl`, `SortPointDistanse`, `idEmployer`, `PublishedAt`, `Created_At`, `Archived`, `ApplyAlternateUrl`, `idInsiderInterview`, `Url`, `AlternateUrl`, `idSnippet`, `idContacts`) VALUES
(33928980, 0, 'Брокер по недвижимости', NULL, 0, 0, 53, 10, 0, 3205119, NULL, NULL, 2711518, '2020-01-20 11:14:53', '2020-01-20 11:14:53', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=33928980', NULL, 'https://api.hh.ru/vacancies/33928980?host=hh.ru', 'https://hh.ru/vacancy/33928980', 10, 10),
(34103590, 0, 'Продавец-консультант (ТЦ РИО)', NULL, 0, 0, 2033, 4, 0, 679675, NULL, NULL, 1025275, '2020-01-09 10:00:35', '2020-01-09 10:00:35', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=34103590', NULL, 'https://api.hh.ru/vacancies/34103590?host=hh.ru', 'https://hh.ru/vacancy/34103590', 4, 4),
(34742160, 0, 'Менеджер направления (товары широкого потребления) - АШАН Боровая', NULL, 0, 0, 2, NULL, 0, 1588276, NULL, NULL, 54979, '2019-12-26 10:46:14', '2019-12-26 10:46:14', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=34742160', NULL, 'https://api.hh.ru/vacancies/34742160?host=hh.ru', 'https://hh.ru/vacancy/34742160', 3, NULL),
(35050898, 0, 'Руководитель проекта', NULL, 0, 0, 1, NULL, 0, NULL, NULL, NULL, 80, '2019-12-18 15:58:45', '2019-12-18 15:58:45', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=35050898', NULL, 'https://api.hh.ru/vacancies/35050898?host=hh.ru', 'https://hh.ru/vacancy/35050898', 5, NULL),
(35146691, 0, 'Директор филиала (г. Кемерово)', NULL, 0, 0, 47, NULL, 0, NULL, NULL, NULL, 3192913, '2019-12-29 01:13:02', '2019-12-29 01:13:02', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=35146691', NULL, 'https://api.hh.ru/vacancies/35146691?host=hh.ru', 'https://hh.ru/vacancy/35146691', 8, NULL),
(35165680, 0, 'Продавец-консультант (г. Кудымкар)', NULL, 0, 0, 1328, 9, 0, 2825975, NULL, NULL, 2537115, '2020-01-20 09:05:55', '2020-01-20 09:05:55', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=35165680', NULL, 'https://api.hh.ru/vacancies/35165680?host=hh.ru', 'https://hh.ru/vacancy/35165680', 9, 9),
(35194655, 0, 'Директор магазина Uterque', NULL, 0, 0, 1, NULL, 0, NULL, NULL, NULL, 20327, '2020-01-09 11:03:08', '2020-01-09 11:03:08', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=35194655', NULL, 'https://api.hh.ru/vacancies/35194655?host=hh.ru', 'https://hh.ru/vacancy/35194655', 1, NULL),
(35266911, 0, 'Личный водитель', NULL, 0, 0, 160, NULL, 0, NULL, NULL, NULL, 569201, '2020-01-13 12:24:13', '2020-01-13 12:24:13', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=35266911', NULL, 'https://api.hh.ru/vacancies/35266911?host=hh.ru', 'https://hh.ru/vacancy/35266911', 6, 6),
(35301817, 0, 'Генеральный директор', NULL, 0, 0, 2759, NULL, 0, NULL, NULL, NULL, 2800, '2020-01-17 18:36:42', '2020-01-17 18:36:42', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=35301817', NULL, 'https://api.hh.ru/vacancies/35301817?host=hh.ru', 'https://hh.ru/vacancy/35301817', 7, 7),
(35335879, 0, 'Шеф-повар', NULL, 0, 0, 1, 2, 0, NULL, NULL, NULL, 3549703, '2020-01-15 18:14:30', '2020-01-15 18:14:30', 0, 'https://hh.ru/applicant/vacancy_response?vacancyId=35335879', NULL, 'https://api.hh.ru/vacancies/35335879?host=hh.ru', 'https://hh.ru/vacancy/35335879', 2, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`idAddress`);

--
-- Индексы таблицы `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`idArea`);

--
-- Индексы таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`idContacts`),
  ADD KEY `FK_contacts_phones_idPhones` (`idPhones`);

--
-- Индексы таблицы `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`idDepartment`);

--
-- Индексы таблицы `employer`
--
ALTER TABLE `employer`
  ADD PRIMARY KEY (`idEmployer`),
  ADD KEY `FK_employer_logo_urls_idLogoUrls` (`idLogoUrls`);

--
-- Индексы таблицы `insider_interview`
--
ALTER TABLE `insider_interview`
  ADD PRIMARY KEY (`idInsiderInterview`);

--
-- Индексы таблицы `logo_urls`
--
ALTER TABLE `logo_urls`
  ADD PRIMARY KEY (`idLogoUrls`);

--
-- Индексы таблицы `metro`
--
ALTER TABLE `metro`
  ADD PRIMARY KEY (`idMetro`),
  ADD KEY `FK_metro_address_idAddress` (`idAddress`);

--
-- Индексы таблицы `metrostations`
--
ALTER TABLE `metrostations`
  ADD PRIMARY KEY (`idMetroStations`),
  ADD KEY `FK_metrostations_address_idAddress` (`idAddress`);

--
-- Индексы таблицы `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`idPhones`);

--
-- Индексы таблицы `salary`
--
ALTER TABLE `salary`
  ADD PRIMARY KEY (`idSalary`);

--
-- Индексы таблицы `snippet`
--
ALTER TABLE `snippet`
  ADD PRIMARY KEY (`idSnippet`);

--
-- Индексы таблицы `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`idType`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `vacancies`
--
ALTER TABLE `vacancies`
  ADD PRIMARY KEY (`idVacancies`),
  ADD KEY `FK_vacancies_address_idAddress` (`idAddress`),
  ADD KEY `FK_vacancies_area_idArea` (`idArea`),
  ADD KEY `FK_vacancies_contacts_idContacts` (`idContacts`),
  ADD KEY `FK_vacancies_department_idDepartment` (`idDepartment`),
  ADD KEY `FK_vacancies_employer_idEmployer` (`idEmployer`),
  ADD KEY `FK_vacancies_insider_interview_idInsiderInterview` (`idInsiderInterview`),
  ADD KEY `FK_vacancies_salary_idSalary` (`idSalary`),
  ADD KEY `FK_vacancies_snippet_idSnippet` (`idSnippet`),
  ADD KEY `FK_vacancies_type_idType` (`idType`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `FK_contacts_phones_idPhones` FOREIGN KEY (`idPhones`) REFERENCES `phones` (`idPhones`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `employer`
--
ALTER TABLE `employer`
  ADD CONSTRAINT `FK_employer_logo_urls_idLogoUrls` FOREIGN KEY (`idLogoUrls`) REFERENCES `logo_urls` (`idLogoUrls`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `metro`
--
ALTER TABLE `metro`
  ADD CONSTRAINT `FK_metro_address_idAddress` FOREIGN KEY (`idAddress`) REFERENCES `address` (`idAddress`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `metrostations`
--
ALTER TABLE `metrostations`
  ADD CONSTRAINT `FK_metrostations_address_idAddress` FOREIGN KEY (`idAddress`) REFERENCES `address` (`idAddress`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `vacancies`
--
ALTER TABLE `vacancies`
  ADD CONSTRAINT `FK_vacancies_address_idAddress` FOREIGN KEY (`idAddress`) REFERENCES `address` (`idAddress`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_vacancies_area_idArea` FOREIGN KEY (`idArea`) REFERENCES `area` (`idArea`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_vacancies_contacts_idContacts` FOREIGN KEY (`idContacts`) REFERENCES `contacts` (`idContacts`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_vacancies_department_idDepartment` FOREIGN KEY (`idDepartment`) REFERENCES `department` (`idDepartment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_vacancies_employer_idEmployer` FOREIGN KEY (`idEmployer`) REFERENCES `employer` (`idEmployer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_vacancies_insider_interview_idInsiderInterview` FOREIGN KEY (`idInsiderInterview`) REFERENCES `insider_interview` (`idInsiderInterview`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_vacancies_salary_idSalary` FOREIGN KEY (`idSalary`) REFERENCES `salary` (`idSalary`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_vacancies_snippet_idSnippet` FOREIGN KEY (`idSnippet`) REFERENCES `snippet` (`idSnippet`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_vacancies_type_idType` FOREIGN KEY (`idType`) REFERENCES `type` (`idType`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
