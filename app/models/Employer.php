<?php

class Employer extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idEmployer;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $url;

    /**
     *
     * @var string
     */
    public $alternateUrl;

    /**
     *
     * @var integer
     */
    public $idLogoUrls;

    /**
     *
     * @var string
     */
    public $vacanciesUrl;

    /**
     *
     * @var integer
     */
    public $trusted;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("hh");
        $this->setSource("employer");
        $this->hasMany('idEmployer', 'Vacancies', 'idEmployer', ['alias' => 'Vacancies']);
        $this->belongsTo('idLogoUrls', 'LogoUrls', 'idLogoUrls', ['alias' => 'LogoUrls']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'employer';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Employer[]|Employer|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Employer|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
