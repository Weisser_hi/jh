<?php

class LogoUrls extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idLogoUrls;

    /**
     *
     * @var string
     */
    public $240;

    /**
     *
     * @var string
     */
    public $90;

    /**
     *
     * @var string
     */
    public $original;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("hh");
        $this->setSource("logo_urls");
        $this->hasMany('idLogoUrls', 'Employer', 'idLogoUrls', ['alias' => 'Employer']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'logo_urls';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return LogoUrls[]|LogoUrls|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return LogoUrls|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
