<?php

class Contacts extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idContacts;

    /**
     *
     * @var string
     */
    public $name;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var integer
     */
    public $idPhones;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("hh");
        $this->setSource("contacts");
        $this->hasMany('idContacts', 'Vacancies', 'idContacts', ['alias' => 'Vacancies']);
        $this->belongsTo('idPhones', 'Phones', 'idPhones', ['alias' => 'Phones']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'contacts';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Contacts[]|Contacts|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Contacts|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
