<?php

class Snippet extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idSnippet;

    /**
     *
     * @var string
     */
    public $requirment;

    /**
     *
     * @var string
     */
    public $responsibility;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("hh");
        $this->setSource("snippet");
        $this->hasMany('idSnippet', 'Vacancies', 'idSnippet', ['alias' => 'Vacancies']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'snippet';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Snippet[]|Snippet|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Snippet|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
