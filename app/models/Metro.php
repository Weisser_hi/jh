<?php

class Metro extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $idMetro;

    /**
     *
     * @var string
     */
    public $station_name;

    /**
     *
     * @var string
     */
    public $line_Name;

    /**
     *
     * @var string
     */
    public $station_Id;

    /**
     *
     * @var integer
     */
    public $line_Id;

    /**
     *
     * @var string
     */
    public $lat;

    /**
     *
     * @var string
     */
    public $lng;

    /**
     *
     * @var integer
     */
    public $idAddress;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("hh");
        $this->setSource("metro");
        $this->belongsTo('idAddress', 'Address', 'idAddress', ['alias' => 'Address']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'metro';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Metro[]|Metro|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Metro|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
