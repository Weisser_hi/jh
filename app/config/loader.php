<?php

$loader = new \Phalcon\Loader();
$loader->registerNamespaces(
    [
        "jh\\frontend\\Controllers" => APP_PATH."/controllers",
        "jh\\frontend\\Models"      => APP_PATH."/models",
        "jh\\frontend\\tools\\validation"   => APP_PATH.'/tools/validation',
        "jh\\frontend\\tools\\plugins"      => APP_PATH.'/tools/plugins',


    ]
);
/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir
    ]
);
    $loader->register();
