function deleteBtnHandler(event){
	//удалить элемент, по согласию пользователя.
	event.preventDefault();
	if(confirm("Удалить элемент?"))
	{
		return location.replace($(this).attr("url"));
	}
}


$("document").ready(function(){
	$(".delete_btn").on("click", deleteBtnHandler);
});